<!--
SPDX-FileCopyrightText: 2021-2024 Citadel and contributors

SPDX-License-Identifier: GPL-3.0-or-later
-->

<p align="center">
  <img height="300" src="https://avatars.githubusercontent.com/u/86734767">
  <h1 align="center">🏰 Hybrid Citadel</h1>
</p>

Hybrid Citadel is a fork of Citadel that is designed to run together with Nirvati. Long-term goal is to remove all code here.
